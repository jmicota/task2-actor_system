#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include "err.h"

extern int sys_nerr;

void syserr(int errcode) {
  (void) errcode;
  exit(1);
}
