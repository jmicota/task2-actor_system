#ifndef ACT_POOL_H
#define ACT_POOL_H

#include "cacti.h"
#include "my_queues.h"
#include <pthread.h>
#include <semaphore.h>
#include <stdbool.h>
#include <stddef.h>

#define ECREATE     140     // bad create
#define EDESTROY    141     // bad destroy
#define EMALLOC     142     // bad malloc
#define EREALLOC    143     // bad realloc
#define ELOCK       144     // bad (un)lock

#define ACT_POOL_INIT_SIZE 4

typedef bool flag;

typedef struct actor {
    actor_id_t id;
    role_t* role;
    void **stateptr;

    pthread_mutex_t outer_lock; // exclusive access to mqueue and actors flags
    mqueue_t* mqueue;
    
    flag is_dead;
    flag is_waiting_in_pqueue;
    flag is_busy;
} actor_t;

typedef struct act_pool {
    actor_t** act_arr;
    size_t act_size;
    size_t act_n;
    size_t dead_n;
    pthread_mutex_t lock_pool;
} act_pool_t;

typedef struct act_sys {
    flag all_dead;
    flag sigint_terminated;
    size_t curr_working_threads;
    pthread_mutex_t mutex_sys_state; // mutex for data above

    pthread_attr_t attr;
    pthread_t* th_arr;
    pthread_t sigint_th;
    pthread_key_t th_key;
    act_pool_t* act_pool;

    pqueue_t* pqueue;
    pthread_mutex_t mutex_pqueue;
    sem_t sem_let_pqueue;

    sem_t sem_join;
    size_t sem_join_counter;
    pthread_mutex_t lock_sem_join_counter;
} act_sys_t;

act_pool_t* act_pool_create(role_t* role, actor_id_t* actor, int* err);

void act_pool_free(act_pool_t* pool);

actor_id_t new_act(act_pool_t* pool, role_t* role);

actor_t* get_actor(actor_id_t id, act_pool_t* pool);

void set_thread_data(act_sys_t* sys, actor_id_t id);

bool system_dead(act_sys_t* sys);

bool system_sigint(act_sys_t* sys);

void set_sigint(act_sys_t* sys);

void set_system_dead(act_sys_t* sys);

void lock_pqueue(act_sys_t* sys);

void unlock_pqueue(act_sys_t* sys);

void lock_join_count(act_sys_t* sys);

void unlock_join_count(act_sys_t* sys);

void lock_pool(act_pool_t* pool);

void unlock_pool(act_pool_t* pool);

void outer_lock_act(actor_t* actor);

void outer_unlock_act(actor_t* actor);

void lock_sys_state(act_sys_t* sys);

void unlock_sys_state(act_sys_t* sys);

bool system_dead(act_sys_t* sys);

void process_message(actor_t* actor, message_t* message, act_sys_t* sys);

#endif