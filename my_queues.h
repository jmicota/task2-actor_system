#ifndef MY_QUEUES_H
#define MY_QUEUES_H

#include "err.h"
#include "cacti.h"
#include "my_queues.h"
#include <stdbool.h>
#include <stddef.h>

#ifndef PQUEUE_INIT_SIZE
#define PQUEUE_INIT_SIZE 500
#endif

typedef struct mqueue {
    message_t** arr;
    size_t max_n;
    size_t n;
    size_t begin;
    size_t end;
} mqueue_t;

typedef struct pqueue {
    actor_id_t* arr;
    size_t max_n;
    size_t n;
    size_t begin;
    size_t end;
} pqueue_t;

actor_id_t pqueue_pop_actor(pqueue_t* q);

void pqueue_push(actor_id_t id, pqueue_t* q);

bool pqueue_empty(pqueue_t* q);

pqueue_t* pqueue_create();

void pqueue_destroy(pqueue_t* q);

size_t pqueue_size(pqueue_t* q);

bool mqueue_empty(mqueue_t* q);

size_t mqueue_size(mqueue_t* q);

message_t* mqueue_pop(mqueue_t* q);

void mqueue_push(message_t* message, mqueue_t* q);

mqueue_t* mqueue_create(size_t size);

void mqueue_destroy(mqueue_t* q);

#endif