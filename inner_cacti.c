#include "my_queues.h"
#include "err.h"
#include "cacti.h"
#include "inner_cacti.h"
#include <pthread.h>
#include <semaphore.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

void lock_pqueue(act_sys_t* sys) {
    if (pthread_mutex_lock(&sys->mutex_pqueue) != 0)
        syserr (ELOCK);
}

void unlock_pqueue(act_sys_t* sys) {
    if (pthread_mutex_unlock(&sys->mutex_pqueue) != 0)
        syserr (ELOCK);
}

void lock_pool(act_pool_t* pool) {
    if (pthread_mutex_lock(&pool->lock_pool) != 0)
        syserr (ELOCK);
}

void unlock_pool(act_pool_t* pool) {
    if (pthread_mutex_unlock(&pool->lock_pool) != 0)
        syserr (ELOCK);
}

void outer_lock_act(actor_t* act) {
    if (pthread_mutex_lock(&act->outer_lock) != 0)
        syserr (ELOCK);
}

void outer_unlock_act(actor_t* act) {
    if (pthread_mutex_unlock(&act->outer_lock) != 0)
        syserr (ELOCK);
}

void lock_sys_state(act_sys_t* sys) {
    if (pthread_mutex_lock(&sys->mutex_sys_state) != 0)
        syserr (ELOCK);
}

void unlock_sys_state(act_sys_t* sys) {
    if (pthread_mutex_unlock(&sys->mutex_sys_state) != 0)
        syserr (ELOCK);
}

void lock_join_count(act_sys_t* sys) {
    if (pthread_mutex_lock(&sys->lock_sem_join_counter) != 0)
        syserr (ELOCK);
}

void unlock_join_count(act_sys_t* sys) {
    if (pthread_mutex_unlock(&sys->lock_sem_join_counter) != 0)
        syserr (ELOCK);
}

bool system_dead(act_sys_t* sys) {
    lock_sys_state(sys);
    bool state = (sys->all_dead);
    unlock_sys_state(sys);
    return state;
}

bool system_sigint(act_sys_t* sys) {
    lock_sys_state(sys);
    bool state = (sys->sigint_terminated);
    unlock_sys_state(sys);
    return state;
}

void set_system_dead(act_sys_t* sys) {
    lock_sys_state(sys);
    sys->all_dead = true;
    unlock_sys_state(sys);
}

void set_sigint(act_sys_t* sys) {
    lock_sys_state(sys);
    sys->sigint_terminated = true;
    unlock_sys_state(sys);
}

void set_thread_data(act_sys_t* sys, actor_id_t id) {
    int err;
    actor_t* actor = get_actor(id, sys->act_pool);
    if ((err = pthread_setspecific(sys->th_key, (&actor->id))) < 0)
        syserr(ECREATE);
}

static void act_pool_resize(act_pool_t* pool) {
    pool->act_arr = realloc(pool->act_arr, pool->act_size * 2 * sizeof(actor_t*));
    if (pool->act_arr == NULL)
        syserr(EREALLOC);
    pool->act_size *= 2;
}

void execute_godie(actor_t* actor) {
    outer_lock_act(actor);
    actor->is_dead = true;
    outer_unlock_act(actor);
}

void execute_spawn(actor_t* actor, message_t* message, act_sys_t* sys) {
    actor_id_t new_actor_id = new_act(sys->act_pool, message->data);
    // message_t* m = malloc(sizeof(message_t));
    // m->message_type = MSG_HELLO;
    // m->nbytes = 0;
    // m->data = &(actor->id);
    // send_message(new_actor_id, *m);
    // free(m);
    message_t m;
    m.message_type = MSG_HELLO;
    m.nbytes = 0;
    m.data = &(actor->id);
    send_message(new_actor_id, m);
}

void execute_message(actor_t* actor, message_t* message) {
    if ((size_t)message->message_type < actor->role->nprompts)
        (actor->role->prompts)[message->message_type](actor->stateptr, message->nbytes, message->data);
}

void process_message(actor_t* actor, message_t* message, act_sys_t* sys) {
    if (message != NULL) {
        if (message->message_type == MSG_GODIE)
            execute_godie(actor);
        else if (message->message_type == MSG_SPAWN && !system_sigint(sys))
            execute_spawn(actor, message, sys);
        else
            execute_message(actor, message);
        free(message);
    }
}

actor_id_t new_act(act_pool_t* pool, role_t* role_arg) {
    int err = 0;
    lock_pool(pool);

    if (pool->act_n >= CAST_LIMIT)
        syserr(ENOMEM);
    if (pool->act_n >= pool->act_size - 1)
        act_pool_resize(pool);
    
    actor_t* actor = malloc(sizeof(actor_t));
    if (actor == NULL)
        syserr(EMALLOC);
    if ((err = pthread_mutex_init(&actor->outer_lock, 0)) != 0)
        syserr(ECREATE);

    actor->id = pool->act_n;
    actor->role = role_arg;
    pool->act_arr[pool->act_n] = actor;
    (pool->act_n)++;
    actor->mqueue = mqueue_create(ACTOR_QUEUE_LIMIT);
    if (actor->mqueue == NULL)
        syserr(EMALLOC);
    actor->stateptr = malloc(sizeof(void*));
    *actor->stateptr = NULL;
    actor->is_dead = false;
    actor->is_waiting_in_pqueue = false;
    actor->is_busy = false;

    unlock_pool(pool);
    return actor->id;
}

actor_t* get_actor(actor_id_t id, act_pool_t* pool) {
    lock_pool(pool);
    if ((size_t)id >= pool->act_n) {
        unlock_pool(pool);
        return NULL;
    }
    actor_t* a = (pool->act_arr)[id];
    unlock_pool(pool);
    return a;
}

static void actor_free(act_pool_t* pool, size_t id) {
    if (pool->act_arr[id] != NULL) {
        mqueue_destroy(pool->act_arr[id]->mqueue);
        if (pool->act_arr[id]->stateptr != NULL)
            free(pool->act_arr[id]->stateptr);
        if (pthread_mutex_destroy (&(pool->act_arr[id])->outer_lock) != 0)
            syserr (EDESTROY);
        free(pool->act_arr[id]);
    }
}

act_pool_t* act_pool_create(role_t* role, actor_id_t* actor, int* err) {
    act_pool_t* pool = malloc(sizeof(act_pool_t));
    if (pool == NULL) 
        syserr(EMALLOC);
    pool->act_arr = malloc(ACT_POOL_INIT_SIZE * sizeof(actor_t));
    if (pool->act_arr == NULL) 
        syserr(EMALLOC);
    if ((*err = pthread_mutex_init(&pool->lock_pool, 0)) != 0)
        syserr(ECREATE);
    
    pool->act_size = ACT_POOL_INIT_SIZE;
    pool->act_n = 0;
    pool->dead_n = 0;
    *actor = new_act(pool, role);
    return pool;
}

void act_pool_free(act_pool_t* pool) {
    for (size_t i = 0; i < pool->act_n; i++) {
        if (pool->act_arr[i] != NULL)
            actor_free(pool, i);
    }
    pthread_mutex_destroy (&pool->lock_pool);
    free(pool->act_arr);
    free(pool);
}