#include "cacti.h"
#include "my_queues.h"
#include "err.h"
#include "inner_cacti.h"
#include <pthread.h>
#include <semaphore.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>

// GLOBAL POINTER TO SYSTEM 
act_sys_t* act_system = NULL;

void check_for_sigint(actor_t* actor) {
    if (system_sigint(act_system))
        actor->is_dead = true;
}

void release_all_threads() {
    for (size_t i = 0; i < POOL_SIZE; i++)
        sem_post(&act_system->sem_let_pqueue);
}

static void sys_free() {
    void *void_ptr;

    pthread_cancel(act_system->sigint_th);
    pthread_join(act_system->sigint_th, &void_ptr);
    release_all_threads();

    for (size_t i = 0; i < POOL_SIZE; i++) {
        pthread_join((act_system->th_arr)[i], &void_ptr);
    }

    sem_destroy(&act_system->sem_join);
    sem_destroy(&act_system->sem_let_pqueue);
    pqueue_destroy(act_system->pqueue);
    act_pool_free(act_system->act_pool);

    pthread_mutex_destroy (&act_system->mutex_sys_state);
    pthread_mutex_destroy (&act_system->mutex_pqueue);
    pthread_mutex_destroy (&act_system->lock_sem_join_counter);
    pthread_attr_destroy (&(act_system->attr));

    free(act_system->th_arr);
    free(act_system);
}

static void *thread_work() {
    size_t dead_nr = 0, act_nr = 0;
    while (true) {
        sem_wait(&(act_system->sem_let_pqueue));

        if (system_dead(act_system) || system_sigint(act_system)) {
            lock_sys_state(act_system);
            act_system->curr_working_threads--;
            size_t curr_working = act_system->curr_working_threads;
            unlock_sys_state(act_system);
            if (curr_working == 0) { // im last thread working
                sem_post(&act_system->sem_join);
            }
            return 0;
        } 

        lock_pqueue(act_system);
        if (pqueue_empty(act_system->pqueue)) {
            unlock_pqueue(act_system);
            syserr(ELOCK);
        }

        actor_id_t* id = malloc(sizeof(actor_id_t));
        *id = pqueue_pop_actor(act_system->pqueue);
        actor_t* act = get_actor(*id, act_system->act_pool);
        if (act == NULL) syserr(ESRCH);

        outer_lock_act(act);
        unlock_pqueue(act_system);

        act->is_busy = true;
        act->is_waiting_in_pqueue = false;
        set_thread_data(act_system, *id);
        free(id);

        message_t* message = mqueue_pop(act->mqueue);
        outer_unlock_act(act);

        process_message(act, message, act_system);
            
        outer_lock_act(act); 
        check_for_sigint(act);    

        size_t q_size = mqueue_size(act->mqueue);
        if (q_size != 0) { // if mqueue not empty always push actor back
            lock_pqueue(act_system);
            pqueue_push(act->id, act_system->pqueue);
            act->is_busy = false;
            act->is_waiting_in_pqueue = true;
            sem_post(&(act_system->sem_let_pqueue));
            unlock_pqueue(act_system);

            outer_unlock_act(act);
        }
        else if (!(act->is_dead)) { // mqueue empty but actor alive => leave him
            act->is_busy = false;
            outer_unlock_act(act);
        }
        else {
            act->is_busy = false;

            lock_pool(act_system->act_pool);
            (act_system->act_pool->dead_n)++;
            dead_nr = act_system->act_pool->dead_n;
            act_nr = act_system->act_pool->act_n;
            // lock_sys_state => making sure that thread leaves actor mutex before publicly
            // claiming actors death in system state (before start of system cleanup)
            lock_sys_state(act_system);
            unlock_pool(act_system->act_pool);
            outer_unlock_act(act);
            unlock_sys_state(act_system);

            if (dead_nr == act_nr) { // last actor dies
                set_system_dead(act_system);
                release_all_threads();
            }
        }
    }
    return 0;
}

void handle_sigint() {
    set_sigint(act_system);
    release_all_threads();
}

void *wait_for_sigint() {
    int sig;
    pthread_setcanceltype(PTHREAD_CANCEL_ENABLE, NULL);

    struct sigaction sigint_handler;
    sigint_handler.sa_handler = handle_sigint;
    sigint_handler.sa_flags = 0;
    sigemptyset(&sigint_handler.sa_mask);
    sigaction(SIGINT, &sigint_handler, NULL);

    sigwait(&sigint_handler.sa_mask, &sig);
    return 0;
}

static int sys_create(size_t n, actor_id_t *actor, role_t *const role) {
    int err;
    act_system = malloc(sizeof(act_sys_t));
    if (act_system == NULL)
        return -1;

    act_system->th_arr = malloc(n * sizeof(pthread_t));
    if (act_system->th_arr == NULL)
        return -1;
        
    act_system->all_dead = false;
    act_system->sigint_terminated = false;
    act_system->act_pool = act_pool_create(role, actor, &err);
    act_system->pqueue = pqueue_create();
    act_system->sem_join_counter = 0;
    act_system->curr_working_threads = POOL_SIZE;
    
    if (act_system->act_pool == NULL)
        return -1;
    if ((err = pthread_mutex_init(&act_system->mutex_sys_state, 0)) != 0)
        return err;
    if ((err = pthread_mutex_init(&act_system->mutex_pqueue, 0)) != 0)
        return err;
    if(sem_init(&act_system->sem_let_pqueue, 0, 0))
        return -1;
    if(sem_init(&act_system->sem_join, 0, 0))
        return -1;
    if ((err = pthread_mutex_init(&act_system->lock_sem_join_counter, 0)) != 0)
        return err;
    if ((err = pthread_attr_init (&(act_system->attr))) != 0)
        return err;
    if ((err = pthread_attr_setdetachstate (&(act_system->attr), PTHREAD_CREATE_JOINABLE)) != 0)
        return err;
    if ((err = pthread_key_create(&act_system->th_key, NULL)) != 0)
        return err;
    if ((err = pthread_setspecific(act_system->th_key, NULL)) < 0)
        return err;

    sigset_t block_mask;
    sigemptyset(&block_mask);
    sigaddset(&block_mask, SIGINT); 

    if ((err = pthread_create(&(act_system->sigint_th), &(act_system->attr), wait_for_sigint, 0)) != 0)
        return err;
    if ((err = pthread_sigmask(SIG_BLOCK, &block_mask, NULL) != 0))
        return err;
    for (size_t i = 0; i < n; i++) {
        if ((err = pthread_create(&((act_system->th_arr)[i]), &(act_system->attr), thread_work, 0)) != 0)
            return err;
    }
    message_t msghello;
    msghello.message_type = MSG_HELLO;
    msghello.nbytes = sizeof(size_t);
    msghello.data = (void*)actor;
    if ((err = send_message(*actor, msghello) != 0))
        return err;

    return 0;
}

int actor_system_create(actor_id_t *actor, role_t *const role) {
    int err = sys_create(3, actor, role);
    if (err != 0 && err > 0)
        err *= -1;
    return err;
}

int send_message(actor_id_t actor, message_t message) {

    if (act_system == NULL) // system destroyed or not created
        return -1;
    if (system_dead(act_system))
        return -1;

    lock_pool(act_system->act_pool);
    if ((size_t)actor >= act_system->act_pool->act_n || actor < 0) {
        unlock_pool(act_system->act_pool);
        return -2;
    }
    else unlock_pool(act_system->act_pool);
        
    actor_t* act = get_actor(actor, act_system->act_pool);    
    outer_lock_act(act); // exclusive access to messages and flags

    bool is_busy = act->is_busy;
    bool is_waiting_in_pqueue = act->is_waiting_in_pqueue;
    if (act->is_dead) {
        outer_unlock_act(act);
        return -1;  
    }
    if (mqueue_size(act->mqueue) >= ACTOR_QUEUE_LIMIT) {
        outer_unlock_act(act);
        return -3;  
    }

    message_t* m = malloc(sizeof(message_t));
    *m = message; // copy
    mqueue_push(m, act->mqueue);

    if (!is_busy && !is_waiting_in_pqueue) {
        lock_pqueue(act_system);
        pqueue_push(act->id, act_system->pqueue);
        act->is_waiting_in_pqueue = true;
        sem_post(&(act_system->sem_let_pqueue));
        unlock_pqueue(act_system);
    }

    outer_unlock_act(act);
    return 0;
}

void actor_system_join(actor_id_t id) { // last thread joining frees memory

    if (act_system != NULL) {
        lock_pool(act_system->act_pool);
        if ((size_t)id < act_system->act_pool->act_n) {
            unlock_pool(act_system->act_pool);
            
            lock_join_count(act_system);
            (act_system->sem_join_counter)++;
            unlock_join_count(act_system);

            sem_wait(&act_system->sem_join); // wait for system finish

            lock_join_count(act_system);
            (act_system->sem_join_counter)--;
            if (act_system->sem_join_counter == 0) { // im last
                unlock_join_count(act_system);
                sys_free();
            }
            else {
                unlock_join_count(act_system);
                sem_post(&act_system->sem_join);
            }
        }
        else unlock_pool(act_system->act_pool);
    }
}

actor_id_t actor_id_self() {
    actor_id_t id = *((actor_id_t*)pthread_getspecific(act_system->th_key));
    return id;
}