#include "cacti.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

typedef long long data_t;

role_t act_role;

void suicide() {
    message_t godie;
    godie.message_type = MSG_GODIE;
    send_message(actor_id_self(), godie);
}

void f0_hello(void **stateptr, size_t nbytes, void *data) {
    (void) stateptr;
    (void) nbytes;
    actor_id_t father = *((actor_id_t*) data);
    if (father != actor_id_self()) { // if i have a parent, send him my id
        message_t m;
        m.message_type = 1;
        m.nbytes = sizeof(actor_id_t);
        actor_id_t* self = malloc(sizeof(actor_id_t));
        *self = actor_id_self();
        m.data = (actor_id_t*)self;
        send_message(father, m);
    }
}

void f1_calculate_and_pass(void **stateptr, size_t nbytes, void *data) {
    (void) nbytes;
    data_t* calculations = (data_t*)(*stateptr);
    actor_id_t child_id = *((actor_id_t*) data);
    free(data);

    if (calculations[0] == calculations[2]) { // calculations finished
        printf("%lld\n", calculations[1]);
        message_t die_child;
        die_child.message_type = MSG_GODIE;
        die_child.nbytes = 0;
        die_child.data = NULL;
        send_message(child_id, die_child);
    }
    else { // pass calculations to child (init)
        calculations[2]++;
        calculations[1] = calculations[1] * calculations[2];

        message_t init;
        init.message_type = 2;
        init.nbytes = sizeof(calculations);
        init.data = (void*) calculations;
        send_message(child_id, init);
    }
    suicide();
}

// spawn new actor and receive calculations
void f2_init(void **stateptr, size_t nbytes, void *data) {
    (void) nbytes;
    *stateptr = (void*) (data_t*)data;
    data_t* calculations = (data_t*)(*stateptr);

    if (calculations[0] < 0) {
        suicide();
    }
    else if (calculations[0] == 0) {
        printf("%d\n", 1);
        suicide();
    }
    else {
        message_t msgspawn;
        msgspawn.message_type = MSG_SPAWN;
        msgspawn.nbytes = sizeof(role_t);
        msgspawn.data = (void*) &act_role;
        send_message(actor_id_self(), msgspawn);
    }
}

int main()  {
    actor_id_t act;

    act_t tab[4] = {&f0_hello, &f1_calculate_and_pass, &f2_init};
    act_role.nprompts = 3;
    act_role.prompts = tab;

    actor_system_create(&act, &act_role);

    data_t k;
    scanf("%lld", &k);
    data_t calculations[3] = {k, 1, 1};

    message_t init;
    init.message_type = 2;
    init.nbytes = sizeof(calculations);
    init.data = (void*) &calculations;
    
    send_message(act, init);
    actor_system_join(act);

    return 0;
}