#include "cacti.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// field of matrix
typedef struct field {
	long int value;
	long int complexity;
} field_t;

// struct passed between actors during calculations
typedef struct calculation {
    size_t row;
    long int sum;
} calc_t;

typedef struct matrix {
	size_t columns;
	size_t rows;
    field_t** fields;
	actor_id_t* children_ids;
    size_t nr_of_children;
} matrix_t;

role_t act_role;
matrix_t* matrix;

void suicide() {
    message_t godie;
    godie.message_type = MSG_GODIE;
    send_message(actor_id_self(), godie);
}

void f0_hello(void **stateptr, size_t nbytes, void *data) {
    (void) nbytes;
    actor_id_t father = *((actor_id_t*) data);

    if (father == actor_id_self()) { // spawn |column| number of actors
        *stateptr = 0; // nr of children that responded after spawn
        message_t msgspawn;
        msgspawn.message_type = MSG_SPAWN;
        msgspawn.nbytes = sizeof(role_t*);
        msgspawn.data = (void*) &act_role;

        for (size_t i = 0; i < matrix->columns; i++) {
            send_message(actor_id_self(), msgspawn);
        }
    }
    else { // pass my id to my parent
        message_t m;
        m.message_type = 1;
        m.nbytes = sizeof(actor_id_t);
        actor_id_t* self = malloc(sizeof(actor_id_t));
        *self = actor_id_self();
        m.data = (actor_id_t*)self;
        send_message(father, m);
    }
}

// receive child id, write it down, pass nr of column to child responsible for it
void f1_get_child(void **stateptr, size_t nbytes, void *data) {
    (void) nbytes;
    actor_id_t child_id = *((actor_id_t*) data);
    free(data);
    matrix->children_ids[(size_t)*stateptr] = child_id;

    message_t pass_column;
    pass_column.message_type = 3;
    pass_column.nbytes = sizeof(size_t);
    size_t* col = malloc(sizeof(size_t));
    *col = (size_t)*stateptr;
    pass_column.data = (size_t*)col;
    send_message(child_id, pass_column);

    // increment number of children that responded to hello with their ids
    (*stateptr)++; 

    if ((size_t)*stateptr == matrix->columns) {
        message_t start;
        start.message_type = 2;
        start.nbytes = sizeof(calc_t);
        calc_t *calc = malloc(sizeof(calc_t));
        calc->row = 0;
        calc->sum = 0;
        start.data = (calc_t*)calc;
        send_message(matrix->children_ids[0], start);
        suicide();
    }
}

void f2_count_row(void **stateptr, size_t nbytes, void *data) {
    (void) nbytes;
    calc_t* c = (calc_t*)data;
    size_t my_column = *((size_t*)*stateptr);

    if (c->row >= matrix->rows) { // if all rows done, initiate killing actors
        for (size_t i = 0; i < matrix->columns; i++) {
            message_t mdie;
            mdie.message_type = 4;
            send_message(matrix->children_ids[i], mdie);
        }
        free(c);
    }
    else {
        size_t i = matrix->columns * c->row + my_column;
        usleep((matrix->fields[i])->complexity);
        c->sum += (matrix->fields[i])->value;

        // if actor responsible for last column, print result and reset calculations
        if (matrix->children_ids[matrix->columns - 1] == actor_id_self()) {
            printf("%ld\n", c->sum);
            c->sum = 0;
            c->row++;
            message_t m;
            m.message_type = 2;
            m.nbytes = sizeof(calc_t);
            m.data = data;
            send_message(matrix->children_ids[0], m);
        }
        else {
            message_t m;
            m.message_type = 2;
            m.nbytes = sizeof(calc_t);
            m.data = data;
            send_message(matrix->children_ids[my_column + 1], m);
        }
    }
}

// receive and remember my column number from parent 
void f3_get_column(void **stateptr, size_t nbytes, void *data) {
    (void) nbytes;
    *stateptr = (size_t*)data;
}

void f4_die(void **stateptr, size_t nbytes, void *data) {
    (void) nbytes;
    (void) data;
    free(*stateptr);
    suicide();
}

int main()  {
    actor_id_t act;
    act_t tab[5] = {&f0_hello, &f1_get_child, &f2_count_row, &f3_get_column, &f4_die};
    act_role.nprompts = 5;
    act_role.prompts = tab;

    long int rows, columns;
    scanf("%ld", &rows);
	scanf("%ld", &columns);
	matrix = malloc(sizeof(matrix_t));
    matrix->rows = rows;
    matrix->columns = columns;
    matrix->fields = malloc(rows * columns * sizeof(field_t*));
    matrix->children_ids = malloc(columns * sizeof(size_t));
	for (long int i = 0; i < rows * columns; i++) {
		field_t* f = malloc(sizeof(field_t));
		scanf("%ld", &f->value);
		scanf("%ld", &f->complexity);
		matrix->fields[i] = f;
	}

    actor_system_create(&act, &act_role);

    actor_system_join(act);

	for (long int i = 0; i < rows * columns; i++) {
		free(matrix->fields[i]);
	}
    free(matrix->fields);
    free(matrix->children_ids);
	free(matrix);

    return 0;
}