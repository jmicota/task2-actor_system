#include "err.h"
#include "my_queues.h"
#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

bool pqueue_empty(pqueue_t* q) {
    if (q == NULL)
        syserr(EFAULT);
    return q->n == 0;
}

size_t pqueue_size(pqueue_t* q) {
    if (q == NULL)
        syserr(EFAULT);
    return q->n;
}

actor_id_t pqueue_pop_actor(pqueue_t* q) {
    if (q == NULL)
        syserr(EFAULT);
    actor_id_t id = (q->arr)[q->begin];
    q->begin = (q->begin + 1) % q->max_n;
    (q->n)--;
    return id;
}

void pqueue_resize(pqueue_t* q) {
    q->arr = realloc(q->arr, 2 * q->max_n * sizeof(actor_id_t));
    if (q->arr == NULL)
        syserr(EFAULT);
    
    if (q->end != 0 && q->n > 0) {
        size_t j = q->max_n;
        for (size_t i = q->begin; i < q->end; i++) {
            (q->arr)[j] = (q->arr)[i];
            i++; j++;
        }
    }
    q->end = q->begin + q->n;
    q->max_n *= 2;
}

void pqueue_push(actor_id_t id, pqueue_t* q) {
    if (q->n >= q->max_n)
        pqueue_resize(q);
    (q->arr)[q->end] = id;
    q->end = (q->end + 1) % q->max_n;
    (q->n)++;
}

pqueue_t* pqueue_create() {
    pqueue_t* q = malloc(sizeof(pqueue_t));
    q->n = 0;
    q->arr = malloc(4 * sizeof(actor_id_t));
    q->max_n = 4;
    q->begin = 0;
    q->end = 0;
    return q;
}

void pqueue_destroy(pqueue_t* q) {
    if (q == NULL)
        syserr(EFAULT);
    free(q->arr);
    free(q);
}

bool mqueue_empty(mqueue_t* q) {
    if (q == NULL)
        syserr(EFAULT);
    return q->n == 0;
}

size_t mqueue_size(mqueue_t* q) {
    if (q == NULL)
        syserr(EFAULT);
    return q->n;
}

message_t* mqueue_pop(mqueue_t* q) {
    message_t* m = NULL;
    if (mqueue_empty(q))
        syserr (EFAULT);
    m = (q->arr)[q->begin];
    q->begin = (q->begin + 1) % q->max_n;
    (q->n)--;    
    return m;
}

void mqueue_push(message_t* message, mqueue_t* q) {
    if (q->n >= q->max_n)
        syserr(ENOMEM);
    (q->arr)[q->end] = message;
    q->end = (q->end + 1) % q->max_n;
    (q->n)++;
}

mqueue_t* mqueue_create(size_t size) {
    mqueue_t* q = malloc(sizeof(mqueue_t));
    q->n = 0;
    q->arr = malloc(size * sizeof(message_t*));
    q->max_n = size;
    q->begin = 0;
    q->end = 0;
    return q;
}

void mqueue_destroy(mqueue_t* q) {
    if (q != NULL) {
        for (size_t i = q->begin; i < q->end; i++) {
            if ((q->arr)[i] != NULL)
                free((q->arr)[i]);
        }
        free(q->arr);
        free(q);
    }
}

